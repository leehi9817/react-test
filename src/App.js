import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        Smatori
      </header>
      <Nav variant="underline" defaultActiveKey="/home">
        <Nav.Item>
          <Nav.Link href="/home">Dashboard</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="disabled" disabled>
            안녕하세요, test@example 님
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Button variant="signoutBtn">Sign out</Button>
        </Nav.Item>
      </Nav>
    </div>
  );
}

export default App;
